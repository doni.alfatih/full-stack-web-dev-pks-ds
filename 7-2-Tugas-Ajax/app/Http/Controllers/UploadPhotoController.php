<?php

namespace App\Http\Controllers;

use App\Post;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UploadPhotoController extends Controller
{
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'photo' => 'required|mimes:jpg,jpeg,png,gif'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $post = Post::findOrFail($id);

        if ($request->hasFile('photo')) {

            $image = $request->file('photo');
            $image_extension = $image->getClientOriginalExtension();
            $image_name = $post->id . "." . $image_extension;
            $image_folder = '/photos/blog/';
            $image_location =  $image_folder . $image_name;
    
            try {
              $image->move(public_path($image_folder) , $image_name);
    
              $post->update([
                'photo' => $image_location,
              ]);
    
            } catch (\Exception $e) {
              return response()->json([
                'response_code'    => '01',
                'response_message' => 'photo gagal upload' ,
                'data'    => $post
              ], 200);
            }
            
            return response()->json([
                'success' => true,
                'message' => 'Photo berhasil diupload',
                'data'    => $post  
            ], 200);
        }
    }
}
