<?php

namespace App\Http\Controllers;

use App\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{    
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table roles
        $roles = Roles::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Roles',
            'data'    => $roles  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
        $roles = Roles::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Role',
            'data'    => $roles 
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $roles = Roles::create([
            'name'     => $request->name,
        ]);

        //success save to database
        if($roles) {

            return response()->json([
                'success' => true,
                'message' => 'Role Created',
                'data'    => $roles  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Post Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $roles
     * @return void
     */
    public function update(Request $request, Roles $roles)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $roles = Roles::findOrFail($roles->id);

        if($roles) {

            //update roles
            $roles->update([
                'name'     => $request->name,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Role Updated',
                'data'    => $roles  
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Role Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $roles = Roles::findOrfail($id);

        if($roles) {

            //delete post
            $roles->delete();

            return response()->json([
                'success' => true,
                'message' => 'Role Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Role Not Found',
        ], 404);
    }
}