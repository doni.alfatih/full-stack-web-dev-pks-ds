<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Mail\PostAuthorMail;
use Illuminate\Http\Request;
use App\Mail\CommentAuthorMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;


class CommentController extends Controller
    {    
        /**
         * index
         *
         * @return void
         */
        public function index()
        {
            //get data from table comments
            $comment = Comments::latest()->get();
    
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'List Data Comment',
                'data'    => $comment  
            ], 200);
    
        }
        
         /**
         * show
         *
         * @param  mixed $id
         * @return void
         */
        public function show($id)
        {
            //find comments by ID
            $comment = Comments::findOrfail($id);
    
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail Data Comment',
                'data'    => $comment
            ], 200);
    
        }
        
        /**
         * store
         *
         * @param  mixed $request
         * @return void
         */
        public function store(Request $request)
        {
            //set validation
            $validator = Validator::make($request->all(), [
                'content'  => 'required',
                'post_id'  => 'required',
            ]);
            
            //response error validation
            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }
    
            //save to database
            $comment = Comments::create([
                'content'     => $request->content,
                'post_id' => $request->post_id,
            ]);

            //Kirim email kepada pemilik Post
            //Mail::to($comment->post->user->email)->send(new PostAuthorMail($comment));

            //Kirim email kepada pemilik Comment    
           // Mail::to($comment->user->email)->send(new CommentAuthorMail($comment));


    
            //success save to database
            if($comment)
            {
    
                return response()->json([
                    'success' => true,
                    'message' => 'Comment Created',
                    'data'    => $comment  
                ], 201);
    
            } 
    
            //failed save to database
            return response()->json([
                'success' => false,
                'message' => 'Comment Failed to Save',
            ], 409);
    
        }
        
        /**
         * update
         *
         * @param  mixed $request
         * @param  mixed $comment
         * @return void
         */
        public function update(Request $request, Comments $comment)
        {
            //set validation
            $validator = Validator::make($request->all(), [
                'content'   => 'required',
            ]);
            
            //response error validation
            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }
    
            //find comment by ID
            $comment = Comments::findOrFail($comment->id);
    
            if($comment)
            {
                $user = auth()->user();

                if($comment->user_id != $user->id)
                {

                //data post not found
                    return response()->json([
                        'success' => false,
                        'message' => 'Akses Anda ditolak, silahkan daftar/login',
                    ], 403);
                } 

                //update comment
                $comment->update([
                    'content'     => $request->content,
                ]);
    
                return response()->json([
                    'success' => true,
                    'message' => 'Comment Updated',
                    'data'    => $comment  
                ], 200);
    
            }
    
            //data comment not found
            return response()->json([
                'success' => false,
                'message' => 'Comment Not Found',
            ], 404);
    
        }
        
        /**
         * destroy
         *
         * @param  mixed $id
         * @return void
         */
        public function destroy($id)
        {
            //find post by ID
            $comment = Comments::findOrfail($id);
    
            if($comment) {

                $user = auth()->user();

                if($comment->user_id != $user->id)
                {

                //data post not found
                    return response()->json([
                        'success' => false,
                        'message' => 'Akses Anda ditolak, silahkan daftar/login',
                    ], 403);
                } 
    
                //delete post
                $comment->delete();
    
                return response()->json([
                    'success' => true,
                    'message' => 'Comment Deleted',
                ], 200);
    
            }
    
            //data comment not found
            return response()->json([
                'success' => false,
                'message' => 'Comment Not Found',
            ], 404);
        }
    }
