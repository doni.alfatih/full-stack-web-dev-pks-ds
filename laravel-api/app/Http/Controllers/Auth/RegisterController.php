<?php

namespace App\Http\Controllers\Auth;

use App\Users;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\RegisterAuthorMail;
use App\Mail\RegenerateAuthorMail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'name'   => 'required',
            'username' => 'required|unique:users,username',
            'email' => 'required|unique:users,email|email'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = Users::create($allRequest);

        do {
            $random = mt_rand(100000 , 999999);
            $check = OtpCode::where('otp',$random)->first();

        } while ($check);

        $now = Carbon::now();

        $otp_code = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(5),
            'user_id' => $user->id

        ]);

        //Kirim email kepada User yang baru saja register
        //Mail::to($user->otp_code->email)->send(new RegisterAuthorMail($user));

        //Kirim email kepada User yang sudah Regenerate OTP
       // Mail::to($user->otp_code->email)->send(new RegenerateAuthorMail($user));


        return response()->json([
            'success' => true,
            'mesaage' => 'Data User berhasil dibuat',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]

        ]);
    }    
}

//Register Akun
//Regenerate Otp Code
//Verifikasi Akun 
//Change Password
//Login
//Memproteksi method Store, Update, dan Delete pada PostController dari user yang tidak login
//Memproteksi method Store, Update, dan Delete pada CommentController dari user yang tidak login