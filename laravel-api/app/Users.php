<?php

namespace App;

use App\Roles;
use App\OtpCode;
use App\Comments;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $fillable = [
      'name' , 'username' , 'email' , 'role_id' , 'password' , 'email_verified_at'
    ];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating( function($model){
            if( empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }

            $model->role_id = Roles::where('name' , 'author')->first()->id;

        });
    }

    public function role()
    {
        return $this->belongsTo('App\Roles');
    }

    public function otp_code()
    {
        return $this->hasOne('App\OtpCode','user_id');
    }

    public function comment()
    {
        return $this->hasMany('App\Comments');
    }

    public function post()
    {
        return $this->hasMany('App\Post');
    }
}
