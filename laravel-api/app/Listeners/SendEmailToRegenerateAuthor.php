<?php

namespace App\Listeners;

use App\Events\UserStoredEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailToRegenerateAuthor
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserStoredEvent  $event
     * @return void
     */
    public function handle(UserStoredEvent $event)
    {
        //
    }
}
