//Jawaban Soal 1
console.log("\nJawaban Soal 1")
    //Luas persegi
        const int = (sisi) => {
          return{
            "Luas Persegi": sisi * sisi,
            "Keliling Persegi" : sisi + sisi + sisi +sisi
          }
        }
        console.log(int(10))

//Jawaban Soal 2
console.log("\nJawaban Soal 2")

const newFunction = (firstName, lastName) =>{
    return {
      fullName:`${firstName} ${lastName}`
    }
}
  
console.log(newFunction("William", "Imoh").fullName) 


//Jawaban Soal 3
console.log("\nJawaban Soal 3")

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

const {firstName, lastName, address, hobby} = newObject

console.log(firstName,lastName,address,hobby)


//Jawaban Soal No 4
console.log("\nJawaban Soal 4")

const west = ['Will', 'Chris', 'Sam', 'Holly']
const east = ['Gill', 'Brian', 'Noel', 'Maggie']
let combined = [...west, ...east]

console.log(combined)

//Jawaban Soal 5
console.log("\nJawaban Soal 5")

const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}` 

console.log(before)